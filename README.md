# MarketKarma.com [![Netlify Status](https://api.netlify.com/api/v1/badges/1ff44bcf-c4ba-4ed6-86d8-90c905cc0705/deploy-status)](https://app.netlify.com/sites/marketkarma/deploys)

- URL: https://www.marketkarma.com
- Sitemap (XML): https://www.marketkarma.com/sitemap.xml
- Last Update: 2022-04-25 08:49:53

## Usage

To update, execute the commands below from the project root:

- `bundle install`
- `s3_website install`
- `s3_website push`
